/*global module:false*/
module.exports = function(grunt) {
 	'use strict';

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    bower: grunt.file.readJSON('./.bowerrc'),

	config: {
		css: 'assets/css',
		js: 'assets/js',
		sass: 'sass',
  		src: 'assets/src',
  		mediaelement: '<%= bower.directory %>/mediaelement/build'
	},
	copy: {
		target: {
			files: [{
			  src: '<%= config.mediaelement %>/mediaelement-flash-audio-ogg.swf',
			  dest: '<%= config.src %>/mediaelement-flash-audio-ogg.swf'
			},
			{
			  src: '<%= config.mediaelement %>/mediaelement-flash-audio.swf',
			  dest: '<%= config.src %>/mediaelement-flash-audio.swf'
			},
			{
			  src: '<%= config.mediaelement %>/mediaelement-flash-video-hls.swf',
			  dest: '<%= config.src %>/mediaelement-flash-flash-video-hls.swf'
			},
			{
			  src: '<%= config.mediaelement %>/mediaelement-flash-video-mdash.swf',
			  dest: '<%= config.src %>/mediaelement-flash-flash-video-mdash.swf'
			},
			{
			  src: '<%= config.mediaelement %>/mediaelement-flash-video.swf',
			  dest: '<%= config.src %>/mediaelement-flash-flash-video.swf'
			},
			{
			  src: '<%= config.mediaelement %>/mejs-controls.png',
			  dest: '<%= config.src %>/mejs-controls.png'
			},
			{				
			  src: '<%= config.mediaelement %>/mejs-controls.svg',
			  dest: '<%= config.src %>/mejs-controls.svg'
			}]
		}
	},

	uglify: {
			target: {
				files: {
					'<%= config.src %>/player.min.js': [
						'<%= bower.directory %>/mediaelement/build/mediaelement-and-player.js',
						'<%= bower.directory %>/mediaelement/build/renderers/dailymotion.js',
						'<%= bower.directory %>/mediaelement/build/renderers/facebook.js',
						'<%= bower.directory %>/mediaelement/build/renderers/soundcloud.js',
						'<%= bower.directory %>/mediaelement/build/renderers/twitch.js',
						'<%= bower.directory %>/mediaelement/build/renderers/vimeo.js',
						'<%= bower.directory %>/mediaelement/build/lang/cs.js',
						'<%= bower.directory %>/mediaelement/build/lang/de.js',
						'<%= bower.directory %>/mediaelement/build/lang/es.js',
						'<%= bower.directory %>/mediaelement/build/lang/fa.js',
						'<%= bower.directory %>/mediaelement/build/lang/fr.js',
						'<%= bower.directory %>/mediaelement/build/lang/hr.js',
						'<%= bower.directory %>/mediaelement/build/lang/hu.js',
						'<%= bower.directory %>/mediaelement/build/lang/it.js',
						'<%= bower.directory %>/mediaelement/build/lang/ja.js',
						'<%= bower.directory %>/mediaelement/build/lang/ko.js',
						'<%= bower.directory %>/mediaelement/build/lang/nl.js',
						'<%= bower.directory %>/mediaelement/build/lang/pl.js',
						'<%= bower.directory %>/mediaelement/build/lang/pt.js',
						'<%= bower.directory %>/mediaelement/build/lang/ro.js',
						'<%= bower.directory %>/mediaelement/build/lang/ru.js',
						'<%= bower.directory %>/mediaelement/build/lang/sk.js',
						'<%= bower.directory %>/mediaelement/build/lang/sv.js',
						'<%= bower.directory %>/mediaelement/build/lang/uk.js',
						'<%= bower.directory %>/mediaelement/build/lang/zh-cn.js',
						'<%= bower.directory %>/mediaelement/build/lang/zh.js'
					],
					'<%= config.src %>/app.min.js': [
						'<%= config.js %>/app.js'
					]
				}
			}
		},
		sass: {
			dist: {
				options: {
					compass: true,
					style: 'compressed'
				},
				files: {                        
					'<%= config.css %>/app.css': '<%= config.sass %>/app.scss'
				}
			}
		},
		cssmin: {
			target: {
				files: {
					'<%= config.src %>/player.min.css': [
						'<%= bower.directory %>/mediaelement/build/mediaelementplayer.min.css',
						'<%= config.css %>/app.css'
					]
				}
			}
		}
  });

  // These plugins provide necessary tasks.
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	//grunt.loadNpmTasks('grunt-contrib-watch');
	//grunt.loadNpmTasks('grunt-cache-breaker');

  // Default task.
	grunt.registerTask('default', ['copy', 'uglify', 'sass', 'cssmin']);
	//grunt.registerTask('deploy', ['cachebreaker']);
	
	//grunt.registerTask( 'w', [ 'watch' ] );

};
